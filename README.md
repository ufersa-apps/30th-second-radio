# :arrow_forward: 30th Second Radio :notes:

This is a simple  Music Player made with Javascript, pure HTML and CSS.


It also uses the [Deezer](https://www.deezer.com/br/) API through the [Rapid API](https://docs.rapidapi.com/) service.

With it, you can :mag: search and enjoy some free music demos ( :clock3: 30 seconds).